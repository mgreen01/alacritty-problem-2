import itertools

# People Class Declartion
class People:
    # ID Counter
    id_gen = itertools.count()
    Name = str()
    Age = int()
    # Class Attributes Declartion
    def __init__(self, Name, Age):
        self.id = next(People.id_gen)
        self.Name = Name
        self.Age = Age

# Pre-coded class objects
s1 = People('Alice', 20)
s2 = People('Bob', 25)
s3 = People('Carol', 30)
s4 = People('Dave', 35)

print(s1.id, s1.Name, s1.Age)
print(s2.id, s2.Name, s2.Age)
print(s3.id, s3.Name, s3.Age)
print(s4.id, s4.Name, s4.Age)

print("\n")

Full_Count=(s4.id+1)
Age_Sum=(s1.Age+s2.Age+s3.Age+s4.Age)



print(Full_Count)
print(Age_Sum / 4)